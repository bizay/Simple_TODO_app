<html>
    <head>
        <title>Create new Task</title>
    </head>
    <body>
        <form action="{{isset($tasks) ? '/edit/'.$tasks->id: route('new')}}" method="POST">
            {{csrf_field()}}
            New Task: <input type="text" value="{{ isset($tasks) ? $tasks->body : '' }}" name="body"  ><br/>
            <hr>
            <button type="submit">{{isset($tasks) ? 'Update Task': 'Create Task'}}</button>
        </form>

    <ul>
        @foreach($errors->all() as $error)
            <li>{{$error}}</li>
        @endforeach
    </ul>
        <a href="{{route('master')}}">Go home</a>
    </body>

</html>