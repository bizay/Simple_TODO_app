@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <form method="get">
                    <input type="text" name="q" placeholder="Search the Task">
                    <button type="search">Search</button>
                </form>
                <hr>
                <table class="table">
                    <thead>
                        <th>ID</th>
                        <th>Date</th>
                        <th>Task Detail</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </thead>
                    @foreach($tasks as $task)
                        <tr>
                            <td width>{{$task->id}}</td>
                            <td width>{{$task->created_at->diffForHumans() }}</td>
                            <td>{{$task->body}}</td>
                            <td><a href="/edit/{{$task->id}}">edit</a></td>
                            <td><a href="/delete/{{$task->id}}">delete</a></td>
                        </tr>
                    @endforeach



                </table>
                <hr>
                <a href="/createTask">
                Create New Task
                </a>
            </div>
        </div>
    </div>
</div>

@endsection