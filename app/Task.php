<?php

namespace App;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Task extends Eloquent
{
protected $fillable = [
    'body',
    'user_id'
];

protected $hidden = [
    'id'
];

//    public function user()
//    {
//        return $this->belongsTo('App/User');
//    }
}
