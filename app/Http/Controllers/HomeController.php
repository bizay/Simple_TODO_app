<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Task;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        $tasks = Task::where('user_id',auth()->id())->get();
//        return view('master', compact('tasks'));


        $keyword = $request->input('q');
        if(isset($keyword)){
            $tasks = Task::where('body', 'LIKE', "%$keyword%")->
            where('user_id',auth()->id())->get();
        }else{
            $tasks = Task::where('user_id',auth()->id())->get();
        }

        return view('master', compact('tasks'));
    }
}
