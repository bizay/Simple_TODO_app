<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Task;

class TasksController extends Controller
{
   public function index()
   {

//       $keyword = Input::get('q');
//       if(isset($keyword)){
//           $tasks = Task::where('body', 'LIKE', "%$keyword%")->get();
//       }else{
//           $tasks = Task::all();
//       }
//
//       return view('master', compact('tasks'));
   }
   public function create()
   {
       return view('new');
   }

    public function store()
    {

        $this->validate(request(),[
           'body' => 'required|min:2'
        ]);

        Task::create([
            'body'=> request('body'),
            'user_id'=> auth()->id()

            ]);

        return redirect("/");
    }

    public function edit($id)
    {

        $tasks =Task::find($id);
        return view('new', compact('tasks'));

    }

    public function update($id)
    {

        $this->validate(request(),[
            'body' => 'required|min:2'
        ]);

        $tasks =Task::find($id);
        $tasks->body= request('body');
        $tasks->save();

        return redirect("/");
    }

    public function delete($id)
    {
            $tasks =Task::find($id);
            $tasks->delete();

            return redirect("/");
    }

    public function api_task()
    {
        dd(Task::all()->count());
    }

}
