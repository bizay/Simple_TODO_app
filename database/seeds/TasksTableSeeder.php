<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;


class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            $faker = Faker\Factory::create();
    	foreach (range(1,10) as $index) {
            DB::table('tasks')->insert([
                'body' => $faker->text,
                'user_id' => $faker->numberBetween(1,2),
                'created_at' => $faker->DateTime,
                'updated_at' => $faker->DateTime,
            ]);

        }

    }
}
