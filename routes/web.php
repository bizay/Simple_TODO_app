<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('master');

Auth::routes();


Route::middleware(['auth'])->group( function (){

Route::get('createTask', 'TasksController@create');

Route::get('/edit/{id}', 'TasksController@edit')->name('edit');

Route::get('delete/{id}', 'TasksController@delete');

Route::post('/new', 'TasksController@store')->name('new');

Route::post('/edit/{id}', 'TasksController@update')->name('edit');

Route::get('api/task', 'TasksController@api_task');



Route::get('/{q}', 'TasksController@index');
});





